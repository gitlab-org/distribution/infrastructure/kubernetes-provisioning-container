FROM alpine as base

ARG KUBERNETES_VERSION="1.28.4"
ARG HELM_VERSION="3.11.3"
ARG GCLOUD_SDK_VERSION="502.0.0"

RUN apk add --no-cache jq wget curl bash python3 git

# Helm
RUN curl --retry 6 -Ls "https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz" | tar -xz -C /tmp/ \
    && chmod +x /tmp/linux-amd64/helm \
    && mv /tmp/linux-amd64/helm /usr/local/bin/helm

# Kubectl
RUN curl -o /usr/local/bin/kubectl -L "https://dl.k8s.io/release/v${KUBERNETES_VERSION}/bin/linux/amd64/kubectl" \
    && chmod +x /usr/local/bin/kubectl


# Gcloud
WORKDIR /opt
RUN curl -O "https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-${GCLOUD_SDK_VERSION}-linux-x86_64.tar.gz" \
    && tar -xf "google-cloud-sdk-${GCLOUD_SDK_VERSION}-linux-x86_64.tar.gz" \
    && rm "google-cloud-sdk-${GCLOUD_SDK_VERSION}-linux-x86_64.tar.gz" \
    && cd google-cloud-sdk \
    && ./install.sh -q

RUN /opt/google-cloud-sdk/bin/gcloud components install gke-gcloud-auth-plugin

FROM alpine

COPY --from=base / /
ENV PATH=${PATH}:/opt/google-cloud-sdk/bin
